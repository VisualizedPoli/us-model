var electorateStatuses = {
"Auckland Central": {winner: "labour", incumbent: "national"},
"Banks Peninsula": {winner: "labour", incumbent: "labour"},
"Bay of Plenty": {winner: "national", incumbent: "national"},
"Botany": {winner: "national", incumbent: "national"},
"Christchurch Central": {winner: "labour", incumbent: "labour"},
"Christchurch East": {winner: "labour", incumbent: "labour"},
"Coromandel": {winner: "national", incumbent: "national"},
"Dunedin": {winner: "labour", incumbent: "labour"},
"East Coast": {winner: "labour", incumbent: "national"},
"East Coast Bays": {winner: "national", incumbent: "national"},
"Epsom": {winner: "act", incumbent: "act"},
"Hamilton East": {winner: "labour", incumbent: "national"},
"Hamilton West": {winner: "labour", incumbent: "national"},
"Hutt South": {winner: "labour", incumbent: "national"},
"Ilam": {winner: "national", incumbent: "national"},
"Invercargill": {winner: "labour", incumbent: "national"},
"Kaikōura": {winner: "national", incumbent: "national"},
"Kaipara ki Mahurangi": {winner: "national", incumbent: "national"},
"Kelston": {winner: "labour", incumbent: "labour"},
"Mana": {winner: "labour", incumbent: "labour"},
"Māngere": {winner: "labour", incumbent: "labour"},
"Manurewa": {winner: "labour", incumbent: "labour"},
"Maungakiekie": {winner: "labour", incumbent: "national"},
"Mt Albert": {winner: "labour", incumbent: "labour"},
"Mt Roskill": {winner: "labour", incumbent: "labour"},
"Napier": {winner: "labour", incumbent: "labour"},
"Nelson": {winner: "labour", incumbent: "national"},
"New Lynn": {winner: "labour", incumbent: "labour"},
"New Plymouth": {winner: "labour", incumbent: "national"},
"North Shore": {winner: "national", incumbent: "national"},
"Northcote": {winner: "labour", incumbent: "national"},
"Northland": {winner: "national", incumbent: "national"},
"Ōhāriu": {winner: "labour", incumbent: "labour"},
"Ōtaki": {winner: "labour", incumbent: "national"},
"Pakuranga": {winner: "national", incumbent: "national"},
"Palmerston North": {winner: "labour", incumbent: "labour"},
"Panmure-Ōtāhuhu": {winner: "labour", incumbent: "labour"},
"Papakura": {winner: "national", incumbent: "national"},
"Port Waikato": {winner: "national", incumbent: "national"},
"Rangitata": {winner: "labour", incumbent: "national"},
"Rangitīkei": {winner: "national", incumbent: "national"},
"Remutaka": {winner: "labour", incumbent: "labour"},
"Rongotai": {winner: "labour", incumbent: "labour"},
"Rotorua": {winner: "labour", incumbent: "national"},
"Selwyn": {winner: "national", incumbent: "national"},
"Southland": {winner: "national", incumbent: "national"},
"Taieri": {winner: "labour", incumbent: "labour"},
"Takanini ": {winner: "labour", incumbent: "national"},
"Tāmaki": {winner: "national", incumbent: "national"},
"Taranaki-King Country": {winner: "national", incumbent: "national"},
"Taupō": {winner: "national", incumbent: "national"},
"Tauranga": {winner: "national", incumbent: "national"},
"Te Atatū": {winner: "labour", incumbent: "labour"},
"Tukituki": {winner: "labour", incumbent: "national"},
"Upper Harbour": {winner: "national", incumbent: "national"},
"Waikato": {winner: "national", incumbent: "national"},
"Waimakariri": {winner: "national", incumbent: "national"},
"Wairarapa": {winner: "labour", incumbent: "national"},
"Waitaki": {winner: "national", incumbent: "national"},
"Wellington Central": {winner: "labour", incumbent: "labour"},
"West Coast-Tasman": {winner: "labour", incumbent: "labour"},
"Whanganui": {winner: "labour", incumbent: "national"},
"Whangaparāoa": {winner: "national", incumbent: "national"},
"Whangarei": {winner: "labour", incumbent: "national"},
"Wigram": {winner: "labour", incumbent: "labour"},
// Maori electorates
"Hauraki-Waikato": {winner: "labour", incumbent: "labour"},
"Ikaroa-Rāwhiti": {winner: "labour", incumbent: "labour"},
"Tāmaki Makaurau": {winner: "labour", incumbent: "labour"},
"Te Tai Hauāuru": {winner: "labour", incumbent: "labour"},
"Te Tai Tokerau": {winner: "labour", incumbent: "labour"},
"Te Tai Tonga": {winner: "labour", incumbent: "labour"},
"Waiariki": {winner: "labour", incumbent: "labour"}
};

console.log(electorateStatuses);

Array.prototype.forEach.call(
  document.querySelectorAll(".hexagon"),
  function(el, i) {
    var electorate = el.innerHTML;
	//console.log(electorate);
    var partyCode = electorateStatuses[electorate].party || electorateStatuses[electorate].winner;
    el.setAttribute("title", electorate);
    el.setAttribute("data-id", i + 1);
    el.classList.add("party-" + partyCode);

    electorateStatuses[electorate].party = electorateStatuses[electorate].winner;
});

var tip = tippy(".hexagon", {
  position: "right",
  arrow: true,
  interactive: true,
  onShow: function() {
    var hexagon = document.querySelector("[aria-describedby=" + this.id + "]");

    var source = document.querySelector("#dropdown-template").innerText;
    var template = Handlebars.compile(source);
    var electorate = hexagon.innerText;
    var winnerString = electorateStatuses[electorate].winner.charAt(0).toUpperCase() + electorateStatuses[electorate].winner.slice(1);
    var incumbentString = electorateStatuses[electorate].incumbent.charAt(0).toUpperCase() + electorateStatuses[electorate].incumbent.slice(1);
      
    if (electorateStatuses[electorate].winner != electorateStatuses[electorate].incumbent) {
        var status = winnerString + ' GAIN from ' + incumbentString;
    } else {
        var status = winnerString + ' HOLD';
    
    }
    
    hexagon.setAttribute(
      "title",
      template({
        id: hexagon.getAttribute("data-id"),
        electorate: hexagon.innerText,
		status: status
      })
    );
    tip.update(this);
  }
});
