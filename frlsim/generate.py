import frontmatter
import hashlib
from jinja2 import Markup
from markdown import markdown
import os
import subprocess

def generate(core):

	core.render('usa-2020/', 'frlsim/usa-2020/index.html')
	core.render('usa-2020/presidential/', 'frlsim/usa-2020/presidential.html')
	core.render('nz-2020/', 'frlsim/nz-2020/index.html')
	
	# Generate US Model States
	
	states = ["National",
"Alabama",
"Alaska",
"Arizona",
"Arkansas",
"California",
"Colorado",
"Connecticut",
"DC",
"Delaware",
"Florida",
"Georgia",
"Hawaii",
"Idaho",
"Illinois",
"Indiana",
"Iowa",
"Kansas",
"Kentucky",
"Louisiana",
"Maine",
"Maine-1",
"Maine-2",
"Maryland",
"Massachusetts",
"Michigan",
"Minnesota",
"Mississippi",
"Missouri",
"Montana",
"Nebraska",
"Nebraska-1",
"Nebraska-2",
"Nebraska-3",
"Nevada",
"New_Hampshire",
"New_Jersey",
"New_Mexico",
"New_York",
"North_Carolina",
"North_Dakota",
"Ohio",
"Oklahoma",
"Oregon",
"Pennsylvania",
"Rhode_Island",
"South_Carolina",
"South_Dakota",
"Tennessee",
"Texas",
"Utah",
"Vermont",
"Virginia",
"Washington",
"West_Virginia",
"Wisconsin",
"Wyoming",
"Electoral_Votes"]
	
	for state in states:
		core.render('usa-2020/presidential/{}/'.format(state), 'frlsim/usa-2020/state.html', state=state)

	# Generate NZ Model Electorates
	
	electorates = ["Auckland_Central",
"Banks_Peninsula",
"Bay_of_Plenty",
"Botany",
"Christchurch_Central",
"Christchurch_East",
"Coromandel",
"Dunedin",
"East_Coast",
"East_Coast_Bays",
"Epsom",
"Hamilton_East",
"Hamilton_West",
"Hutt_South",
"Ilam",
"Invercargill",
"Kaikoura",
"Kaipara_ki_Mahurangi",
"Kelston",
"Mana",
"Mangere",
"Manurewa",
"Maungakiekie",
"Mt_Albert",
"Mt_Roskill",
"Napier",
"Nelson",
"New_Lynn",
"New_Plymouth",
"North_Shore",
"Northcote",
"Northland",
"Ohariu",
"Otaki",
"Pakuranga",
"Palmerston_North",
"Panmure-Otahuhu",
"Papakura",
"Port_Waikato",
"Rangitata",
"Rangitikei",
"Remutaka",
"Rongotai",
"Rotorua",
"Selwyn",
"Southland",
"Taieri",
"Takanini",
"Tamaki",
"Taranaki-King_Country",
"Taupo",
"Tauranga",
"Te_Atatu",
"Tukituki",
"Upper_Harbour",
"Waikato",
"Waimakariri",
"Wairarapa",
"Waitaki",
"Wellington_Central",
"West_Coast-Tasman",
"Whanganui",
"Whangaparaoa",
"Whangarei",
"Wigram",
"Hauraki-Waikato",
"Ikaroa-Rawhiti",
"Tamaki_Makaurau",
"Te_Tai_Hauauru",
"Te_Tai_Tokerau",
"Te_Tai_Tonga",
"Waiariki"]
	
	for electorate in electorates:
		core.render('nz-2020/electorate/{}/'.format(electorate), 'frlsim/nz-2020/electorate.html', electorate=electorate)
